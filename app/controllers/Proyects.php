<?php
/**
 * Default class to control the proyect related pages.
 * Uses simpleflash as composer dependency.
 * 
 * @class Proyects
 * @extends controller
 */
use \Tamtamchik\SimpleFlash\Flash;
class Proyects extends Controller
{
  public function __construct()
  {
    if(!isLoggedIn()){
      urlHelper('users/login');
    }

    $this->proyect = $this->model('Proyect');
  }

  /**
   * Proyects main view.
   * 
   * @returns all the information from proyects and its view. 
   */
  public function index()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      if(isset($_POST['created'])){
        $data = $this->proyect->getProyects($_SESSION['user_id'], true);
        if(isset($data[0]))
        {
          $data[0]->all = false;
        }
      } else {
        $data = $this->proyect->getProyects($_SESSION['user_id']);
        if(isset($data[0]))
        {
          $data[0]->all = true;
        }
      }

      $this->view('proyects/index', $data);

    } else {
    $data = $this->proyect->getProyects($_SESSION['user_id']);
    if(isset($data[0]))
    {
      $data[0]->all = true;
    }
    $this->view('proyects/index', $data);
    }
  }

  /**
   * Proyects specific view with all its data.
   *
   * @params proyect id. 
   * @returns selected proyect information and its view. 
   */
  public function show($id)
  {
    $data = $this->proyect->getProyectById($id, $this->proyect->checkWorks($id));

    $this->view('proyects/show', $data);
  }

  /**
   * Proyects create view.
   * This function checks the method used to get into it. If the method used is post, it checks all the values sent
   * to save them at the database otherwise it returns the formulary.
   * 
   * @returns create proyect view and, in case that is needed, the data the user completed. 
   */
  public function add()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      // Sanitize the data that comes by post method
      $args = [
        'name' => FILTER_SANITIZE_STRING,
        'description' => FILTER_SANITIZE_STRING,
      ];

      // Get rid of white spaces
      $data = filter_input_array(INPUT_POST, $args, true);
      $data['image'] = !empty($_FILES) ? $_FILES['image']['name'] : '';

      $data['name'] = trim($data['name'], ' ');
      $data['description'] = trim($data['description'], ' ');

      // Check that none of the fields are wrong or blank
      if(!isset($data['name']) || $data['name'] == ''){ 
        $data['name_err'] = 'You have to enter a name';
      }

      if(!isset($data['description']) || strlen($data['description']) >= 255){ 
        $data['description_err'] = 'You have to enter a valid description';
      }

      if (!empty($data['image'])) {
        try {
          $arrTypes = ["image/jpeg", "image/png", "image/gif"];
          $file = new File($_FILES, $arrTypes);
          
          $file->checkFileErrors();
          $file->saveUploadFile('img/');
  
        } catch (FileException $error) {
          $data['image_err'] = $error->getMessage();
        }
      }

      if(isset($data['name_err']) || isset($data['description_err']) 
      || isset($data['image_err'])){
        $this->view('proyects/add', $data);
      }

      else {
        $data['id_user'] = $_SESSION['user_id'];
        if($this->proyect->add($data)){
          Flash::info('Proyect added.');
          
          urlHelper('proyects/index');
        
        } else {
          Flash::error('There was an unexpected error and the proyect was not added.');
          
          urlHelper('proyects/index');
        }
      }

    } else {
      if(supervisor()){
        $this->view('proyects/add');
      
      } else {
        Flash::error('You do not have those privileges');
          
        urlHelper('proyects/index');
      } 
    }
  }

  /**
   * Proyects update view.
   * This function checks the method used to get into it. If the method used is post, it checks all the values sent
   * to update the selected proyect at the database otherwise it returns the formulary of update with the proyect information.
   * 
   * @params proyect id.
   * @returns update proyect view with its information and, in case that is needed, the data the user completed. 
   */
  public function edit($id)
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      // Sanitize the data that comes by post method
      $args = [
        'name' => FILTER_SANITIZE_STRING,
        'description' => FILTER_SANITIZE_STRING,
        'idproyect' => FILTER_SANITIZE_NUMBER_INT,
      ];

      // Get rid of white spaces
      $data = filter_input_array(INPUT_POST, $args, true);
      $data['name'] = trim($data['name'], ' ');
      $data['description'] = trim($data['description'], ' ');
      $data['id_proyect'] = $id;

      // Check that none of the fields are wrong or blank
      if(!isset($data['name']) || $data['name'] == ''){ 
        $data['name_err'] = 'You have to enter a valid name';
      }

      if(!isset($data['description']) || $data['description'] == '' || strlen($data['description']) >= 255){ 
        $data['description_err'] = 'You have to enter a valid description';
      }

      if(isset($data['name_err']) || isset($data['description_err'])){
        $this->view('proyects/edit', $data);
      }

      else {
        $data['id_user'] = $_SESSION['user_id'];
        if($this->proyect->update($data)){
          Flash::info('Proyect updated.');
          
          urlHelper('proyects/index');
        
        } else {
          Flash::error('There was an unexpected error and the proyect was not updated.');
          
          urlHelper('proyects/index');
        }
      }

    } else {
      $data = $this->proyect->getProyectById($id);
      $data['id_proyect'] = $data[0]->proyect_id;

      if($data[0]->creator_id == $_SESSION['user_id']){
        $this->view('proyects/edit', $data);
      
      } else {
        Flash::error('You do not have those privileges');
          
        urlHelper('proyects/show/' . $id);
      } 
    }
  }

  /**
   * This function checks the method used to get into it. If the method used is post, it deletes the selected proyect and, 
   * in any case, redirects the user into a view from the program, being proyects after delete or home on other cases.
   * 
   * @params proyect id.
   */
  public function delete($id)
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      if($this->proyect->delete($id)){
        Flash::info('Proyect deleted.');

      } else {
        Flash::error('There was an unexpected error and the proyect was not deleted.');
      }

      urlHelper('proyects');
    
    } else {
      urlHelper('home');
    }
  }
}
?>