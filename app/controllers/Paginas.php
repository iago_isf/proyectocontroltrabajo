<?php
/**
 * Default class for the program index and other possible pages such as about or contact.
 * 
 * @class Paginas
 * @extends controller
 */
class Paginas extends Controller
{
    
  public function __construct() 
  {
    
  }

  /**
   * Calls the default set index for the program.
   */
  public function index()
  {
    $this->view('paginas/index.view');
  }
}
?>