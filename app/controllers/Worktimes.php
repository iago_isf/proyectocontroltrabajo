<?php
/**
 * Default class to control the worktime related pages.
 * Uses simpleflash as composer dependency.
 * 
 * @class Worktimes
 * @extends controller
 */
use \Tamtamchik\SimpleFlash\Flash;
class Worktimes extends Controller
{
  public function __construct()
  {
    if(!isLoggedIn()){
      urlHelper('users/login');
    }

    $this->worktime = $this->model('Worktime');
  }

  /**
   * This function creates a new worktime related to the work seting the start time as the time when its
   * executed and the end time as a 0.
   *
   * @params work id.
   */
  public function add($id)
  {
    $workUser = $this->worktime->getUserIdFromWork($id);
    
    if($workUser[0]->iduser == $_SESSION['user_id']){
      $data = $this->worktime->getWorktimesByUser($_SESSION['user_id']);
      $working = false;

      foreach($data as $worktime){
        if($worktime->end == '1000-01-01 00:00:00'){
          $working = true;
        }
      }

      if($working){
        Flash::error('Finnish your active task before starting a new one.');

      } else {
        if($this->worktime->add($id)){
          Flash::info('Time started.');
            
        } else {
          Flash::error('There was an unexpected error, try again.');
        }
      }
    } else {
      Flash::error('You are not able to do this action.');
    }
    
    urlHelper('works/show/' . $id);
  }

   /**
   * This function edits a worktime to set its end time to the current time when its executed.
   * This function checks the user id so no stranger can stop someones timer
   *
   * @params work id, worktime id to be edited and the user id.
   */
  public function edit($work_id, $worktime_id, $user_id)
  {
    if($user_id == $_SESSION['user_id']){
      if($this->worktime->update($worktime_id)){
        Flash::info('Time stopped.');
          
      } else {
        Flash::error('There was an unexpected error, try again.');
      }

    } else {
      Flash::error('You are not able to do this action.');
    }

    urlHelper('works/show/' . $work_id);
  }
}
?>