<?php
/**
 * Default class to control the work related pages.
 * Uses simpleflash as composer dependency.
 * 
 * @class Works
 * @extends controller
 */
use \Tamtamchik\SimpleFlash\Flash;
class Works extends Controller
{
  public function __construct()
  {
    if(!isLoggedIn()){
      urlHelper('users/login');
    }

    $this->work = $this->model('Work');
  }

  /**
   * Works main view.
   * 
   * @returns all the information from works and its view. 
   */
  public function index()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      if(isset($_POST['created'])){
        $data = $this->work->getWorks($_SESSION['user_id'], 1);
        if(isset($data[0]))
        {
          $data[0]->all = 1;
        }

      } else if(isset($_POST['active'])) {
        $data = $this->work->getWorks($_SESSION['user_id'], 2);
        if(isset($data[0]))
        {
          $data[0]->all = 2;
        }

      } else {
        $data = $this->work->getWorks($_SESSION['user_id']);
        if(isset($data[0]))
        {
          $data[0]->all = 0;
        }
      }

      $this->view('works/index', $data);

    } else {
    $data = $this->work->getWorks($_SESSION['user_id']);
    if(isset($data[0]))
    {
      $data[0]->all = 0;
    } else {
      $data[0] = 0;
    }

    $this->view('works/index', $data);
    }
  }

  /**
   * Works specific view with all its data.
   *
   * @params work id. 
   * @returns selected work information and its view. 
   */
  public function show($id)
  {
    $data = $this->work->getWorkById($id);
    $data['worktimes'] = $this->work->getWorkWorktimes($id);

    $this->view('works/show', $data);
  }

  /**
   * Workss create view.
   * This function checks the method used to get into it. If the method used is post, it checks all the values sent
   * to save them at the database otherwise it returns the formulary.
   * 
   * @returns create work view and, in case that is needed, the data the user completed. 
   */
  public function add()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
       // Sanitize the data that comes by post method
       $args = [
        'name' => FILTER_SANITIZE_STRING,
        'description' => FILTER_SANITIZE_STRING,
        'proyect' => FILTER_VALIDATE_INT,
        'user' => FILTER_VALIDATE_INT,
      ];

      // Get rid of white spaces
      $data = filter_input_array(INPUT_POST, $args, true);
      $data['name'] = trim($data['name'], ' ');
      $data['description'] = trim($data['description'], ' ');

      // Check that none of the fields are wrong or blank
      if(!isset($data['name']) || $data['name'] == ''){ 
        $data['name_err'] = 'You have to enter a name';
      }

      if(!isset($data['description']) || strlen($data['description']) >= 100){ 
        $data['description_err'] = 'You have to enter a valid description';
      }

      if(!isset($data['proyect'])){ 
        $data['proyect_err'] = 'You have to choose a proyect';
      }

      if(!isset($data['user'])){ 
        $data['user_err'] = 'You have to choose an user';
      }

      if(isset($data['name_err']) || isset($data['description_err'])
      || isset($data['proyect_err']) || isset($data['user_err'])){
        $data['search']['proyects'] = $this->work->getUserProyects($_SESSION['user_id']);
        $data['search']['users'] = $this->work->getUsers();
        $this->view('works/add', $data);
      }

      else {
        if($this->work->add($data)){
          Flash::info('Task added.');
          
          urlHelper('works/index');
        
        } else {
          Flash::error('There was an unexpected error and the task was not added.');
          
          urlHelper('works/index');
        }
      }

    } else {
      if(supervisor()){
        $data['search']['proyects'] = $this->work->getUserProyects($_SESSION['user_id']);
        $data['search']['users'] = $this->work->getUsers();

        $this->view('works/add', $data);

      } else {
        Flash::error('You do not have those provileges.');
          
        urlHelper('works/index');
      }
    }
  }

  /**
   * Works update view.
   * This function checks the method used to get into it. If the method used is post, it checks all the values sent
   * to update the selected work at the database otherwise it returns the formulary of update with the work information.
   * 
   * @params work id.
   * @returns update work view with its information and, in case that is needed, the data the user completed. 
   */
  public function edit($id)
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      // Sanitize the data that comes by post method
      $args = [
        'name' => FILTER_SANITIZE_STRING,
        'description' => FILTER_SANITIZE_STRING,
      ];

      // Get rid of white spaces
      $data = filter_input_array(INPUT_POST, $args, true);
      $data['name'] = trim($data['name'], ' ');
      $data['description'] = trim($data['description'], ' ');
      $data['id_work'] = $id;

      // Check that none of the fields are wrong or blank
      if(!isset($data['name']) || $data['name'] == ''){ 
        $data['name_err'] = 'You have to enter a name';
      }

      if(!isset($data['description']) || strlen($data['description']) >= 100){ 
        $data['description_err'] = 'You have to enter a valid description';
      }

      if(isset($data['name_err']) || isset($data['description_err'])){

        $this->view('works/add', $data);
      }

      else {
        if($this->work->update($data)){
          Flash::info('Task updated.');
          
          urlHelper('works/index');
        
        } else {
          Flash::error('There was an unexpected error and the task was not added.');
          
          urlHelper('works/index');
        }
      }
    } else {
      $data = $this->work->getWorkById($id);

      $this->view('works/edit', $data);
    } 
  }

  /**
   * This function updates the selected work to mark it as done.
   * 
   * @params work id.
   */
  public function done($id)
  {
    $data = $this->work->getWorkWorktimes($id);
    $check = true;

    foreach($data as $worktime){
      if($worktime->worktime_end == '1000-01-01 00:00:00'){
        $check = false;
      }
    }

    if($check){
      $this->work->done($id);

      Flash::info('Marked as finished.');

    } else {
      Flash::error('There is still work to be done.');
    }
    
    urlHelper('works/show/' . $id);
  }
}
?>