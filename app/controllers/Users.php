<?php
/**
 * Default class to control the user related pages.
 * Uses simpleflash as composer dependency.
 * 
 * @class Users
 * @extends controller
 */
use \Tamtamchik\SimpleFlash\Flash;
class Users extends Controller
{
  public function __construct()
  {
    $this->user = $this->model('User');
  }

  /**
   * Users login view.
   * This function checks the method used to get into it. If the method used is post, it checks all the values sent
   * to contrast the information with the saved at the database.
   * 
   * @returns login view. 
   */
  public function login()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      // Sanitize the data that comes by post method
      $args = [
        'email' => FILTER_SANITIZE_EMAIL,
        'password' => FILTER_SANITIZE_STRING,
      ];

      // Get rid of white spaces
      $data = filter_input_array(INPUT_POST, $args, true);
      $data['email'] = trim($data['email'], ' ');
      $data['password'] = trim($data['password'], ' ');

      // Check that none of the fields are wrong or blank
      if(!isset($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL) || !$this->user->findUserByEmail($data['email'])){
        $data['email_err'] = 'You have to enter a valid email';
      }

      if(!isset($data['password']) || strlen($data['password']) < 6){
        $data['password_err'] = 'Invalid password';
      }

      // If there is any error go again to the register page and show it to the user
      if($data['email_err'] || $data['password_err']){
        $this->view('users/login', $data);
      }

      else {
        // If it is all ok, check the data at the database
        $user = $this->user->login($data);

        if ($user) {
          // If it does match go to session creation
          $this->createUserSession($user);

        } else {
          // If not return to the login
          $data['password_err'] = 'Wrong password';
          $this->view('users/login', $data);
        }
      }

    } else {
      $this->view('users/login');
    } 
  }

  /**
   * This function creates all the necessary cookies for the user and redirects the user to the home page.
   */
  private function createUserSession($user)
  {
    $_SESSION['user_id'] = $user->iduser;
    $_SESSION['user_name'] = $user->name;
    $_SESSION['user_email'] = $user->email;
    $_SESSION['user_supervisor'] = $user->supervisor;

    urlHelper('home');
  }

  /**
   * Users register view.
   * This function checks the method used to get into it. If the method used is post, it checks all the values sent
   * to save them at the database otherwise it returns the formulary.
   * 
   * @returns register view.
   */
  public function register()
  {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      // Sanitize the data that comes by post method
      $args = [
        'name' => FILTER_SANITIZE_STRING,
        'surnames' => FILTER_SANITIZE_STRING,
        'phone' => FILTER_SANITIZE_NUMBER_INT,
        'email' => FILTER_SANITIZE_EMAIL,
        'password' => FILTER_SANITIZE_STRING,
        'confirm_password' => FILTER_SANITIZE_STRING,
      ];

      // Get rid of white spaces
      $data = filter_input_array(INPUT_POST, $args, true);
      $data['name'] = trim($data['name'], ' ');
      $data['surnames'] = trim($data['surnames'], ' ');
      $data['phone'] = trim($data['phone'], ' ');
      $data['email'] = trim($data['email'], ' ');
      $data['password'] = trim($data['password'], ' ');
      $data['confirm_password'] = trim($data['confirm_password'], ' ');

      // Check that none of the fields are wrong or blank
      if(!isset($data['name']) || $data['name'] == ''){ 
        $data['name_err'] = 'You have to enter a name';
      }

      if(!isset($data['surnames']) || $data['surnames'] == ''){ 
        $data['surnames_err'] = 'You have to enter your surnames';
      }

      if(!isset($data['phone']) || strlen($data['phone']) != 9){ 
        $data['phone_err'] = 'You have to enter a valid phone';
      }

      if(!isset($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL) || $this->user->findUserByEmail($data['email'])){
        $data['email_err'] = 'You have to enter an email';
      }

      if(!isset($data['password']) || strlen($data['password']) < 6){
        $data['password_err'] = 'Invalid password';
      }

      if(!isset($data['confirm_password']) || strlen($data['confirm_password']) < 6 || $data['confirm_password'] != $data['password']){
        $data['confirm_password_err'] = 'Passwords doesn´t match';
      }

      // If there is any error go again to the register page and show it to the user
      if($data['name_err'] || $data['surnames_err'] || $data['phone_err']
      || $data['email_err'] || $data['password_err'] || $data['confirm_password_err']){
        $this->view('users/register', $data);
      }

      else {
        // If it is all ok encrypt the password, save the user and redirect to the login
        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        if($this->user->register($data)){
          Flash::info('User registered, you can now login.');
          
          urlHelper('users/login');
        
        } else {
          Flash::error('There was an unexpected error and the user was not registered.');
          
          urlHelper('users/login');
        }
      }

    } else {
      $this->view('users/register');
    } 
  }

  /**
   * This function destroys all the cookies created for the user and redirects the user to the login page.
   */
  public function logout()
  {
    unset($_SESSION['user_id']);
    unset($_SESSION['user_name']);
    unset($_SESSION['user_email']);
    unset($_SESSION['user_supervisor']);

    session_destroy();


    urlHelper('users/login');
  }

  /**
   * Proyects update view.
   * This function checks the method used to get into it. If the method used is post, the user is updated and 
   * redirected in other case the view to become a supervisor is returned.
   * 
   * @returns update view. 
   */
  public function update()
  {
    if(!isLoggedIn()){
      urlHelper('users/login');
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      if(isset($_POST['supervisor'])){
        if($this->user->update($_SESSION['user_id'])){
          Flash::info('Login again to confirm changes.');
          
          urlHelper('logout');
        
        } else {
          Flash::error('There was an unexpected error and changes stayed the same.');
          
          urlHelper('home');
        }

      } else {
        Flash::info('No changes applied.');
          
        urlHelper('home');
      }     

    } else {
      $this->view('users/update');
    }
  }
}
?>