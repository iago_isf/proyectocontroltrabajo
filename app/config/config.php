<?php
/**
 * Global variables used over the program.
 * Uses phpdotenv as composer dependency.
 */
define('APPROOT', $_ENV['APPROOT']);
define('URLROOT', $_ENV['URLROOT']);
define('SITENAME', $_ENV['SITENAME']);

define('DB_HOST', $_ENV['DB_HOST']);
define('DB_NAME', $_ENV['DB_NAME']);
define('DB_USER', $_ENV['DB_USER']);
define('DB_PASS', $_ENV['DB_PASS']);

define('APPVERSION', $_ENV['APPVERSION']);
?>