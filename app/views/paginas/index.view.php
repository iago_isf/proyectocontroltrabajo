<?php
include_once APPROOT . '/views/partials/header.php';
include_once APPROOT . '/views/partials/navbar.php';
?>
<div class="container">
  <div class="flashes mb-3">
    <?= (string) flash() ?>
  </div>
  <div class="row align-items-center">
    <div class="col col-6">
      <img class="w-100" src="https://image.freepik.com/free-vector/tiny-people-project-managers-work-vision-scope-document-vision-scope-document-project-main-plan-project-management-document-concept_335657-2431.jpg" alt="Proyect image">
    </div>
    <div class="col col-6 text-center">
      <h4 class="">Start creating a new proyect and see how your workers do.</h4>
      <a href="<?= URLROOT ?>/proyects" type="button" class="btn btn-outline-dark">Go to proyects</a>
    </div>
  </div>
  <div class="row align-items-center">
    <div class="col col-6 text-center">
      <h4 class="">Lost how much time you have spent doing work? Don't worry, we keep track for you.</h4>
      <a href="<?= URLROOT ?>/works" type="button" class="btn btn-outline-dark">Start working</a>
    </div>
    <div class="col col-6">
      <img class="w-100" src="https://image.freepik.com/free-vector/tiny-businesswoman-checking-completed-tasks-clipboard-flat-illustration_74855-15535.jpg" alt="Task image">
    </div>
  </div>
</div>
<?php
include_once APPROOT . '/views/partials/footer.php';
?>