<?php
include_once APPROOT . '/views/partials/header.php';
?>
<div class="container">
  <div class="row justify-content-start mt-3">
    <div class="col col-4">
      <a href="<?= URLROOT ?>/proyects/show/<?= $data['id_proyect'] ?>" class="btn btn-dark mb-3"><i class="bi bi-arrow-left-short"></i>Go back</a>
    </div>
  </div>
  <div class="row justify-content-center mt-3">
    <div class="col col-4 text-center">
      <h5>Edit proyect</h5>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 mx-auto">
      <form action="<?= URLROOT ?>/proyects/edit/<?= $data['id_proyect'] ?>" method="POST" class="needs-validation" novalidate>
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" name="name" class="form-control mb-3  
        <?php
        if (isset($data['name_err'])) echo 'is-invalid';
        if (isset($data['name']) && $data['name'] != '' && !isset($data['name_err'])) echo 'is-valid';
        ?>" value="<?php if (isset($data['name'])) echo $data['name'];
                    if (isset($data[0]->proyect_name)) echo $data[0]->proyect_name; ?>">
          <div class="invalid-feedback">
            <?php if (isset($data['name_err'])) echo $data['name_err']; ?>
          </div>
        </div>
        <div class="form-group">
          <label for="description">Description</label>
          <textarea type="text" name="description" rows="5" class="form-control mb-3 
        <?php
        if (isset($data['description_err'])) echo 'is-invalid';
        if (isset($data['description']) && $data['description'] != '' && !isset($data['description_err'])) echo 'is-valid';
        ?>"><?php if (isset($data['description'])) echo $data['description'];
            if (isset($data[0]->proyect_description)) echo $data[0]->proyect_description; ?></textarea>
          <div class="invalid-feedback">
            <?php if (isset($data['description_err'])) echo $data['description_err']; ?>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col col-3 text-center">
            <input type="submit" value="update" class="btn btn-dark btn-block w-100">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
include_once APPROOT . '/views/partials/footer.php';
?>