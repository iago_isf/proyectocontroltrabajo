<?php
include_once APPROOT . '/views/partials/header.php';
?>
<div class="container">
  <div class="row justify-content-start mt-3">
    <div class="col col-4">
      <a href="<?= URLROOT ?>/proyects" class="btn btn-dark mb-3"><i class="bi bi-arrow-left-short"></i>Go to proyects</a>
    </div>
  </div>
  <div class="row justify-content-center mt-3">
    <div class="col col-4 text-center">
      <h5>Create a new proyect</h5>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 mx-auto">
      <form action="<?= URLROOT ?>/proyects/add" method="POST" enctype="multipart/form-data" class="needs-validation" novalidate>
        <div class="form-group mb-3">
          <label for="name">Name</label>
          <input type="text" name="name" class="form-control 
          <?php 
          if (isset($data['name_err'])) echo 'is-invalid'; 
          if (isset($data['name']) && $data['name'] != '' && !isset($data['name_err'])) echo 'is-valid'; 
          ?>" 
          value="<?php if (isset($data['name'])) echo $data['name'];?>">
          <div class="invalid-feedback">
            <?php if(isset($data['name_err'])) echo $data['name_err']; ?>
          </div>
        </div>
        <div class="form-group mb-3">
          <label for="description">Description</label>
          <textarea type="text" name="description" rows="5" class="form-control
          <?php 
          if (isset($data['description_err'])) echo 'is-invalid'; 
          if (isset($data['description']) && $data['description'] != '' && !isset($data['description_err'])) echo 'is-valid'; 
          ?>"><?php if (isset($data['description'])) echo $data['description'];?></textarea>
          <div class="invalid-feedback">
            <?php if(isset($data['description_err'])) echo $data['description_err']; ?>
          </div>
        </div>
        <div class="form-group mb-3">
          <label for="image">Proyect image</label>
          <input type="file" name="image" id="image" class="form-control 
          <?= isset($data['image_err']) ? 'is-invalid' : ''?>
          <?php 
            if(isset($data['image']) && $data['image'] != '') echo 'is-valid';
            if(isset($data['image']) && $data['image'] == '') echo 'is-invalid';
            ?>">
          <div class="invalid-feedback">
            <?= isset($data['image_err']) ? $data['image_err'] : ''?>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col col-3 text-center">
            <input type="submit" value="Create" class="btn btn-dark btn-block w-100">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
include_once APPROOT . '/views/partials/footer.php';
?>