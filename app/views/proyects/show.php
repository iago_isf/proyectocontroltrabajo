<?php
include_once APPROOT . '/views/partials/header.php';
?>
<div class="container align-content-center">
  <div class="flashes m-5">
    <?= (string) flash() ?>
  </div>
  <div class="row justify-content-center">
    <div class="col col-12">
      <div class="row justify-content-start">
        <div class="col col-4">
          <a href="<?= URLROOT ?>/proyects" class="btn btn-dark mb-3"><i class="bi bi-arrow-left-short"></i>Go to proyects</a>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col col-12">
          <h4 class="text-center">Proyect: <?= $data[0]->proyect_name ?></h4>
        </div>
      </div>
      <div class="row justify-content-center mt-3">
        <div class="col col-10">
          <p class="">Creator contact: <?= $data[0]->creator_email ?></p>
          <p class="">Description: <?= $data[0]->proyect_description ?></p>
          <?php if(isset($data[0]->proyect_image)){ ?>
          <p class="text-center"><img class="w-25" src="<?= URLROOT . '/public/img/' . $data[0]->proyect_image; ?>" alt="Proyect image"></p>
          <?php } ?>
        </div>
        <div class="col col-2">
          <?php if($data[0]->creator_email == $_SESSION['user_email']){ ?>
          <a href="<?= URLROOT ?>/proyects/edit/<?= $data[0]->proyect_id ?>" class="btn btn-outline-dark w-100">Edit</a>
          <button type="button" class="btn btn-outline-dark w-100 mt-3" data-bs-toggle="modal" data-bs-target="#deleteModal">Delete</button>
          <a href="<?= URLROOT ?>/works/add" class="btn btn-dark w-100 text-white mt-3">Add task</a>
          <?php } ?>
        </div>
      </div>
      <?php if (isset($data[0]->work_id)) { ?>
      <div class="row justify-content-center mt-3">
        <?php foreach ($data as $proyect) { ?>
          <div class="col col-3 mt-3">
            <div class="card h-30">
              <div class="card-body">
                <h4 class="card-title text-center"><?= $proyect->work_name ?></h4>

                <p class="card-text">Description: <?= $proyect->work_description ?></p>
                <p class="card-text">Asigned worker: <?= $proyect->worker_email ?></p>
                <p class="card-text">
                  <a href="<?= URLROOT ?>/works/show/<?= $proyect->work_id ?>" class="btn btn-dark w-100 text-white">Go to task</a>
                </p>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
      <?php } ?>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Are you sure about deleting <?= $data[0]->proyect_name ?>?</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        This changes can't be repaired. It is up to you taking this risk.
      </div>
      <div class="modal-footer">
        <div class="row justify-content-end">
          <button type="button" class="col btn btn-dark" data-bs-dismiss="modal">Close</button>
          <form action="<?= URLROOT ?>/proyects/delete/<?= $data[0]->proyect_id ?>" method="POST" class="col">
            <input type="submit" name="all" value="Understood" class="col btn btn-outline-dark">
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
include_once APPROOT . '/views/partials/footer.php';
?>