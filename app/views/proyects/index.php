<?php
include_once APPROOT . '/views/partials/header.php';
include_once APPROOT . '/views/partials/navbar.php';
?>
<div class="container align-content-center">
  <div class="flashes mb-3 mt-3 w-100">
    <?= (string) flash() ?>
  </div>
  <div class="row justify-content-center">
    <div class="col col-9 pb-3 content-background">
      <div class="row justify-content-center">

        <?php 
        if(isset($data[0]->all)){
          foreach ($data as $proyect) {
        ?>
          <div class="col col-3 mt-3">
            <div class="card h-30">
              <div class="card-body">
                <h4 class="card-title text-capitalize text-center"><?= $proyect->proyect_name ?></h4>

                <div class="card-text">Description:</div>
                <div class="card-text mb-3"><?= $proyect->proyect_description ?></div>
                <div class="card-text">Creator:</div>
                <div class="card-text mb-3"><?= $proyect->user_name . ' ' . $proyect->user_surnames ?></div>
                <p class="card-text">
                  <a href="<?= URLROOT ?>/proyects/show/<?= $proyect->proyect_id ?>" class="btn btn-dark w-100 text-white">Show details</a>
                </p>
              </div>
            </div>
          </div>
        <?php
          } 
        } else {
        ?>
        <div class="col col-12 mt-3 text-center">
          <h4>Unfortunately there are not proyects yet.</h4>
        </div>
        <?php
        }
        ?>

      </div>
    </div>
    <div class="col col-3 text-center p-0">
      <?php if(supervisor()){ ?>
      <div><a href="<?= URLROOT ?>/proyects/add" class="btn btn-outline-dark me-2">Create new proyect</a></div>
      <?php 
      }
      if(isset($data[0]->all)) {
      ?>
      <form action="<?= URLROOT ?>/proyects/" method="POST" class="mt-3 p-3 w-100 <?php if($data[0]->all) echo 'form-background'; ?>">
        <input type="submit" name="all" value="All proyects" class="<?php if(!$data[0]->all) echo 'btn btn-dark';?>">
      </form>
      <?php if(supervisor()){ ?>
      <form action="<?= URLROOT ?>/proyects/" method="POST" class="mt-3 p-3 w-100 <?php if(!$data[0]->all) echo 'form-background'; ?>">
        <input type="submit" name="created" value="My proyects" class="<?php if($data[0]->all) echo 'btn btn-dark';?>">
      </form>
      <?php } } ?>
    </div>
  </div>
</div>
<?php
include_once APPROOT . '/views/partials/footer.php';
?>