<div class="container">
  <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 border-bottom">
    <a href="<?= URLROOT ?>/home" class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-dark text-decoration-none">
      <i class="bi bi-house-fill" style="font-size: 2rem;"></i>
    </a>

    <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
      <li><a href="<?= URLROOT ?>/home" class="nav-link px-2 link-dark <?= isActive('home') ?>">Home</a></li>
      <li><a href="<?= URLROOT ?>/proyects/index" class="nav-link px-2 link-dark <?= isActive('proyects') ?>">Proyects</a></li>
      <li><a href="<?= URLROOT ?>/works/about" class="nav-link px-2 link-dark <?= isActive('works') ?>">Tasks</a></li>
    </ul>

    <div class="col-md-3 text-end">
    <?php 
    if( isLoggedIn() ){ 
      if (!supervisor()){
    ?>
      <a href="<?= URLROOT ?>/users/update" type="button" class="btn btn-outline-dark me-2">Become a supervisor</a>
    <?php } ?>
      <a href="<?= URLROOT ?>/users/logout" type="button" class="btn btn-dark">Logout</a>
    <?php } else { ?>
      <a href="<?= URLROOT ?>/users/login" type="button" class="btn btn-outline-dark me-2">Login</a>
      <a href="<?= URLROOT ?>/users/register" type="button" class="btn btn-dark">Sign-up</a>
    <?php }  ?>
    </div>
  </div>
</div>