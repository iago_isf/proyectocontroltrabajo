  <footer class="bg-dark text-white text-center pt-5 pb-5 mt-3">
    <div class="container">
      <div class="row justify-content-evenly">
        <div class="col">
          <p>Fast links:</p>
          <div>Not having an account? <a href="<?= URLROOT ?>/users/register">Register now</a></div>
          <div>Already have an account? <a href="<?= URLROOT ?>/users/login">Log in</a></div>
          <div>Want to become a supervisor? <a href="<?= URLROOT ?>/users/update">Register now</a></div>
        </div>
        <div class="col">
          <p>Pictures used:</p>
          <div><a href="https://www.freepik.com/vectors/people">People by vectorjuice</a></div>
          <div><a href="https://www.freepik.com/vectors/people">People by pch.vector</a></div>
        </div>
        <div class="col">
          <p>Page created by:</p>
          <div>Iago Senín Fernández</div>
          <div class="row justify-content-center mt-2">
            <a href="" class="col text-white"><i class="bi bi-instagram"></i></a>
            <a href="https://www.linkedin.com/in/iago-senin-fernandez-a42b29225/" class="col text-white"><i class="bi bi-linkedin"></i></a>
            <a href="https://bitbucket.org/iago_isf/proyectocontroltrabajo/src/master/" class="col text-white"><i class="bi bi-git"></i></a>
          </div>
        </div>
      </div>
    </div>
  </footer>
  
  <script type="text/javascript" src="<?= URLROOT ?>/public/js/main.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>
</html>