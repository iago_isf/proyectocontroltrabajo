<?php
include_once APPROOT . '/views/partials/header.php';
?>
  <form action="<?= URLROOT ?>/users/register" method="POST" class="needs-validation position-absolute top-50 start-50 translate-middle border border-3 rounded border-dark p-5" novalidate>
    <div class="form-group mb-3">
      <label for="name">Name</label>
      <input type="text" name="name" class="form-control 
        <?php 
        if (isset($data['name_err'])) echo 'is-invalid'; 
        if (isset($data['name']) && $data['name'] != '' && !isset($data['name_err'])) echo 'is-valid';
        ?>" 
        value="<?php if (isset($data['name'])) echo $data['name'];?>">
      <div class="invalid-feedback">
        <?php if (isset($data['name_err'])) echo $data['name_err'];?>
      </div>
    </div>
    <div class="form-group mb-3">
      <label for="surnames">Surnames</label>
      <input type="text" name="surnames" class="form-control 
        <?php 
        if (isset($data['surnames_err'])) echo 'is-invalid'; 
        if (isset($data['surnames']) && $data['surnames'] != '' && !isset($data['surnames_err'])) echo 'is-valid'; 
        ?>" 
        value="<?php if (isset($data['surnames'])) echo $data['surnames'];?>">
      <div class="invalid-feedback">
        <?= isset($data['surnames_err']) ? $data['surnames_err'] : '' ?>
      </div>
    </div>
    <div class="form-group mb-3">
      <label for="phone">Phone number</label>
      <input type="text" name="phone" class="form-control 
        <?php 
        if (isset($data['phone_err'])) echo 'is-invalid'; 
        if (isset($data['phone']) && $data['phone'] != '' && !isset($data['phone_err'])) echo 'is-valid'; 
        ?>" 
        value="<?php if (isset($data['phone'])) echo $data['phone'];?>">
      <div class="invalid-feedback">
        <?= isset($data['phone_err']) ? $data['phone_err'] : '' ?>
      </div>
    </div>
    <div class="form-group mb-3">
      <label for="email">Email</label>
      <input type="email" name="email" class="form-control 
      <?php 
      if (isset($data['email_err'])) echo 'is-invalid'; 
      if (isset($data['email']) && $data['email'] != '' && !isset($data['email_err'])) echo 'is-valid'; 
      ?>" 
      value="<?php if (isset($data['email'])) echo $data['email'];?>">
      <div class="invalid-feedback">
        <?= isset($data['email_err']) ? $data['email_err'] : '' ?>
      </div>
    </div>
    <div class="form-group mb-3">
      <label for="password">Password</label>
      <input type="password" name="password" class="form-control 
      <?php 
      if (isset($data['password_err'])) echo 'is-invalid'; 
      if (isset($data['password']) && $data['password'] != '' && !isset($data['password_err'])) echo 'is-valid'; 
      ?>" 
      value="<?php if (isset($data['password'])) echo $data['password'];?>">
      <div class="invalid-feedback">
        <?= isset($data['password_err']) ? $data['password_err'] : '' ?>
      </div>
    </div>
    <div class="form-group mb-3">
      <label for="confirm_password">Confirm password</label>
      <input type="password" name="confirm_password" class="form-control 
      <?php 
      if (isset($data['confirm_password_err'])) echo 'is-invalid'; 
      if (isset($data['confirm_password']) && $data['confirm_password'] != '' && !isset($data['confirm_password_err'])) echo 'is-valid'; 
      ?>" 
      value="<?php if (isset($data['confirm_password'])) echo $data['confirm_password'];?>">
      <div class="invalid-feedback">
        <?= isset($data['confirm_password_err']) ? $data['confirm_password_err'] : '' ?>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="<?= URLROOT ?>/users/login">Already have an account? Login now</a>
      </div>
      <div class="col">
        <input type="submit" value="Sign in" class="btn btn-dark btn-block">
      </div>
    </div>
  </form>

  <script type="text/javascript" src="<?= URLROOT ?>/public/js/main.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>
</html>