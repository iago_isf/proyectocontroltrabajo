<?php
include_once APPROOT . '/views/partials/header.php';
?>
<div class="position-absolute top-50 start-50 translate-middle border border-3 rounded border-dark p-5">
  <h6 class="text-center">By confirming this action you will have access to create your own proyects and send tasks to other people.</h6>
  <h6 class="text-center">We reservate our right to suspend all your activity due to bad behaviour.</h6>
  <form action="<?= URLROOT ?>/users/update" method="POST" class="needs-validation mt-3" novalidate>
    <div class="form-check mb-3">
      <label for="supervisor"></label>
      <input type="checkbox" name="supervisor" class="form-check-input">
      <label class="form-check-label" for="flexCheckChecked">
        Confirm the agreement.
      </label>
    </div>
    <div class="row">
      <div class="col col-6 justify content-evenly text-center">
        <input type="submit" value="Confirm" class="btn btn-dark ">
      </div>
      <div class="col col-6 text-center">
        <a href="<?= URLROOT ?>/home" class="btn btn-dark">Cancel</a>
      </div>
    </div>
  </form>
</div>

  <script type="text/javascript" src="<?= URLROOT ?>/public/js/main.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</body>
</html>