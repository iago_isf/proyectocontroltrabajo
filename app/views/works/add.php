<?php
include_once APPROOT . '/views/partials/header.php';
?>
<div class="container">
  <div class="row justify-content-start mt-3">
    <div class="col col-4">
      <a href="<?= URLROOT ?>/works" class="btn btn-dark mb-3"><i class="bi bi-arrow-left-short"></i>Go to tasks</a>
    </div>
  </div>
  <div class="row justify-content-center mt-3">
    <div class="col col-4 text-center">
      <h5>Create a new task</h5>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 mx-auto">
      <form action="<?= URLROOT ?>/works/add" method="POST" class="needs-validation" novalidate>
        <div class="form-group mb-3">
          <label for="proyect">Proyect</label>
          <select class="form-select <?php if (isset($data['proyect_err'])) echo 'is-invalid'; ?>" name="proyect">
            <option disabled selected>Select a proyect</option>
            <?php
            foreach ($data['search']['proyects'] as $proyect) {
            ?>
              <option value="<?= $proyect->proyect_id ?>"><?= $proyect->proyect_name ?></option>
            <?php
            }
            ?>
          </select>
          <div class="invalid-feedback">
            <?php if (isset($data['proyect_err'])) echo $data['proyect_err']; ?>
          </div>
        </div>
        <div class="form-group mb-3">
          <label for="name">Name</label>
          <input type="text" name="name" class="form-control 
        <?php
        if (isset($data['name_err'])) echo 'is-invalid';
        if (isset($data['name']) && $data['name'] != '' && !isset($data['name_err'])) echo 'is-valid';
        ?>" value="<?php if (isset($data['name'])) echo $data['name']; ?>">
          <div class="invalid-feedback">
            <?php if (isset($data['name_err'])) echo $data['name_err']; ?>
          </div>
        </div>
        <div class="form-group mb-3">
          <label for="description">Description</label>
          <textarea type="text" name="description" rows="3" class="form-control
        <?php
        if (isset($data['description_err'])) echo 'is-invalid';
        if (isset($data['description']) && $data['description'] != '' && !isset($data['description_err'])) echo 'is-valid';
        ?>"><?php if (isset($data['description'])) echo $data['description']; ?></textarea>
          <div class="invalid-feedback">
            <?= $data['description_err'] ? $data['description_err'] : '' ?>
          </div>
        </div>
        <div class="form-group mb-3">
          <label for="proyect">User</label>
          <select class="form-select <?php if (isset($data['user_err'])) echo 'is-invalid'; ?>" name="user">
            <option disabled selected>Select a user</option>
            <?php
            foreach ($data['search']['users'] as $user) {
            ?>
              <option value="<?= $user->user_id ?>"><?= $user->user_name . ' ' . $user->user_surnames ?></option>
            <?php
            }
            ?>
          </select>
          <div class="invalid-feedback">
            <?php if (isset($data['user_err'])) echo $data['user_err']; ?>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col col-3 text-center">
            <input type="submit" value="Create" class="btn btn-dark btn-block w-100">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
include_once APPROOT . '/views/partials/footer.php';
?>