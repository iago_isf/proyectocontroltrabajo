<?php
include_once APPROOT . '/views/partials/header.php';
include_once APPROOT . '/views/partials/navbar.php';
?>
<div class="container align-content-center">
  <div class="flashes mb-3">
    <?= (string) flash() ?>
  </div>
  <div class="row justify-content-center">
    <div class="col col-9 content-background pb-3">
      <div class="row justify-content-center">

        <?php
        if(isset($data[0]->all)){
          foreach ($data as $work) {
        ?>
          <div class="col col-3 mt-3">
            <div class="card h-30">
              <div class="card-body">
                <h4 class="card-title text-center">Task: <?= $work->work_name ?></h4>
                <div class="card-text">Proyect:</div>
                <div class="card-text mb-3"><?= $work->proyect_name ?></div>
                <div class="card-text">Description:</div>
                <div class="card-text mb-3"><?= $work->work_description ?></div>
                <p class="card-text">
                  <a href="<?= URLROOT ?>/works/show/<?= $work->work_id ?>" class="btn btn-dark w-100 text-white">See task</a>
                </p>
              </div>
            </div>
          </div>
        <?php
          }
        } else {
        ?>
        <div class="col col-12 mt-3 text-center">
          <h4>Luckily you don't have work.</h4>
        </div>
        <?php } ?>
      </div>
    </div>
    <div class="col col-3 text-center p-0">
      <?php if(supervisor()){ ?>
      <a href="<?= URLROOT ?>/works/add" class="btn btn-outline-dark me-2">Create a new task</a>
      <?php 
      } 
      if(isset($data[0]->all)) {
      ?>
      <form action="<?= URLROOT ?>/works/" method="POST" class="mt-2 p-3 w-100 <?php if($data[0]->all == 0) echo 'form-background';?>">
        <input type="submit" name="mine" value="My tasks" class="<?php if($data[0]->all != 0) echo 'btn btn-dark';?>">
      </form>
      <form action="<?= URLROOT ?>/works/" method="POST" class="mt-2 p-3 w-100 <?php if($data[0]->all == 2) echo 'form-background';?>">
        <input type="submit" name="active" value="Active tasks" class="<?php if($data[0]->all != 2) echo 'btn btn-dark';?>">
      </form>
      <?php if(supervisor()){ ?>
      <form action="<?= URLROOT ?>/works/" method="POST" class="mt-2 p-3 w-100 <?php if($data[0]->all == 1) echo 'form-background';?>">
        <input type="submit" name="created" value="Created tasks" class="<?php if($data[0]->all != 1) echo 'btn btn-dark';?>">
      </form>
      <?php } } else { ?>
      <div class="mt-2 p-3 w-100">
        <a href="<?= URLROOT ?>/works" class="btn btn-dark">My tasks</a>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
<?php
include_once APPROOT . '/views/partials/footer.php';
?>