<?php
include_once APPROOT . '/views/partials/header.php';
?>
<div class="container align-content-center">
  <div class="flashes m-5">
    <?= (string) flash() ?>
  </div>
  <div class="row justify-content-center">
    <div class="col col-12">
      <div class="row justify-content-between">
        <div class="col col-2 text-center">
          <a href="<?= URLROOT ?>/works" class="btn btn-dark mb-3"><i class="bi bi-arrow-left-short"></i>Go to tasks</a>
        </div>
        <div class="col col-2 text-center">
          <?php
          $totaltime = 0;
          foreach ($data['worktimes'] as $worktime) {
            if($worktime->worktime_end != '1000-01-01 00:00:00'){
              $sumtime = strtotime($worktime->worktime_end) - strtotime($worktime->worktime_start);
              $totaltime = $totaltime + $sumtime;
            }
          }

          if($totaltime != 0){
            echo 'Time worked: ' . date('H:i:s', $totaltime);
          }
          ?>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col col-12">
          <h4 class="card-title text-center">Task: <?= $data[0]->work_name ?></h4>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col col-10">
          <p class="card-text">Proyect: <?= $data[0]->proyect_name ?></p>
          <p class="card-text">Description: <?= $data[0]->work_description ?></p>
        </div>
        <div class="col col-2">
          <p><a class="btn btn-outline-dark w-100" href="<?= URLROOT ?>/works/edit/<?= $data[0]->work_id ?>">Edit</a></p>
          <?php
          if ($data[0]->work_done == 1) {
          ?>
            <p class="card-text">Task finished</p>
          <?php
          } else {
          ?>
            <p><a class="btn btn-outline-dark w-100" href="<?= URLROOT ?>/works/done/<?= $data[0]->work_id ?>">Mark as done</a></p>
            <p><a class="btn btn-dark w-100" href="<?= URLROOT ?>/worktimes/add/<?= $data[0]->work_id ?>">Start working</a></p>
          <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col col-9">
      <div class="row justify-content-center border-bottom border-3 border-dark text-center">
        <div class="col col-4">
          Start
        </div>
        <div class="col col-4">
          End
        </div>
        <div class="col col-4">
          Finish
        </div>        
      </div>

      <?php
      if (isset($data['worktimes'][0]->worktime_id)) {
        foreach ($data['worktimes'] as $worktime) {
      ?>
        <div class="row justify-content-center border-bottom border-3 text-center pt-2 pb-2">
          <div class="col col-4">
            <?= $worktime->worktime_start ?>
          </div>
          <div class="col col-4">
            <?php if($worktime->worktime_end == '1000-01-01 00:00:00') echo '-'; else echo $worktime->worktime_end;?>
          </div>
          <div class="col col-4">
          <?php if ($worktime->worktime_end == '1000-01-01 00:00:00') { ?>
            <a href="<?= URLROOT ?>/worktimes/edit/<?= $worktime->work_id ?>/<?= $worktime->worktime_id ?>/<?= $worktime->user_id ?>">Stop working</a>
          <?php } else { echo 'Done'; } ?>
          </div> 
        </div>
      <?php
        }
      }
      ?>
    </div>
  </div>
</div>
<?php
include_once APPROOT . '/views/partials/footer.php';
?>