<?php
include_once APPROOT . '/views/partials/header.php';
?>
<div class="container">
  <div class="row justify-content-start mt-3">
    <div class="col col-4">
      <a href="<?= URLROOT ?>/works/show/<?php if (isset($data['id_work'])) echo $data['id_work']; if (isset($data[0]->work_id)) echo $data[0]->work_id; ?>" class="btn btn-dark mb-3"><i class="bi bi-arrow-left-short"></i>Go back</a>
    </div>
  </div>
  <div class="row justify-content-center mt-3">
    <div class="col col-4 text-center">
      <h5>Edit task</h5>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 mx-auto">
      <form action="<?= URLROOT ?>/works/edit/<?php if (isset($data['id_work'])) echo $data['id_work']; if (isset($data[0]->work_id)) echo $data[0]->work_id; ?>" method="POST" class="needs-validation" novalidate>
        <div class="form-group mb-3">
          <label for="name">Name</label>
          <input type="text" name="name" class="form-control 
        <?php
        if (isset($data['name_err'])) echo 'is-invalid';
        if (isset($data['name']) && $data['name'] != '' && !isset($data['name_err'])) echo 'is-valid';
        ?>" value="<?php if (isset($data['name'])) echo $data['name'];
                    if (isset($data[0]->work_name)) echo $data[0]->work_name; ?>">
          <div class="invalid-feedback">
            <?= $data['name_err'] ? $data['name_err'] : '' ?>
          </div>
        </div>
        <div class="form-group mb-3">
          <label for="description">Description</label>
          <textarea type="text" name="description" rows="3" class="form-control
        <?php
        if (isset($data['description_err'])) echo 'is-invalid';
        if (isset($data['description']) && $data['description'] != '' && !isset($data['description_err'])) echo 'is-valid';
        ?>"><?php if (isset($data['description'])) echo $data['description'];
            if (isset($data[0]->work_description)) echo $data[0]->work_description; ?></textarea>
          <div class="invalid-feedback">
            <?= $data['description_err'] ? $data['description_err'] : '' ?>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col col-3 text-center">
            <input type="submit" value="update" class="btn btn-dark btn-block w-100">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
include_once APPROOT . '/views/partials/footer.php';
?>