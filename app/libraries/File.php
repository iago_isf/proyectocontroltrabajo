<?php
/**
 * This class controls all the steps refered to files.
 * 
 * @class File
 */
class File 
{
  private $file;
  private $types;
  
  /**
   * The constrctor sets the class variables as the user needs.
   * 
   * @params file to be used and archive extensions to avoid
   */
  public function __construct($file, $types)
  {
    $this->file = $file;
    $this->types = $types;
  }

  /**
   * This function checks all the necesary information from a file, like the weight or bad uploads and throws an error if something is wrong.
   */
  public function checkFileErrors()
  {
    if ($this->file['image']['error'] !== UPLOAD_ERR_OK) { 
    
      switch ($this->file['image']['error']) {
        case UPLOAD_ERR_INI_SIZE: 
        case UPLOAD_ERR_FORM_SIZE: 
          throw new FileException('Archive weight out of bounds. Maximun weight: ' . ini_get('upload_max_filesize'));
          break;
        case UPLOAD_ERR_PARTIAL:
          throw new FileException('Could not upload the complete file.');
          break;
        default:
          throw new FileException('Archive upload error.');
          break;
      }
    }
  }

  /**
   * This function saves the file on the especified directory if it passes all the checks for other mistakes like bad file types.
   * 
   * @params directory url
   */
  public function saveUploadFile($url)
  {
    if (in_array($this->file['image']['type'], $this->types) === false) { 
      throw new FileException('Archive extension not supported.');

    } else if (is_uploaded_file($this->file['image']['tmp_name']) === false) { 
      throw new FileException('Archive was not uploaded using a formulary.');

    } else if (move_uploaded_file($this->file['image']['tmp_name'], $url . $this->file['image']['name']) === false) {
      throw new FileException('Error while uploading the file. Could not be saved.');
    }
  }
}
?>