<?php
/**
 * Class to interact with the database using PDO.
 * 
 * @class Database
 */
class Database
{
  private $host = DB_HOST;
  private $user = DB_USER;
  private $pass = DB_PASS;
  private $dbname = DB_NAME;

  private $dbh;
  private $stmt;
  private $error;

  /**
   * When is instancied the class calls the default settings for the database and tries to make a conexion.
   */
  public function __construct()
  {
    $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;

    $options = [
      PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
      PDO::ATTR_PERSISTENT => true,
      PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ];

    try {
      $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);

    } catch (PDOException $e) {
      $this->error = $e->getMessage();
      echo $this->error;
    }
  }

  /**
   * Creates a PDO statement from a user query.
   * 
   * @params SQL query
   */
  public function query(string $SQL)
  {
    $this->stmt = $this->dbh->prepare($SQL);
  }

  /**
   * Checks the data type from a null parameter and sets its type to prevent sql problems and sets its value for the PDO statement.
   * 
   * @params parameter to be set, value of the parameter, data type
   */
  public function bind($param, $value, $type = null)
  {
    if (is_null($type)) {
      switch (true) {
        case is_int($value):    
          $type = PDO::PARAM_INT;
          break;
        case is_bool($value):
          $type = PDO::PARAM_BOOL;
          break;
        case is_null($value):
          $type = PDO::PARAM_NULL;
          break;
        default:
          $type = PDO::PARAM_STR;
      }
    }

    $this->stmt->bindValue($param, $value, $type);
  }
  
  /**
   * Executes the current statement set on the PDO.
   * 
   * @returns execution of the sql statement query
   */
  public function execute()
  {
    return $this->stmt->execute();
  }
  
  /**
   * Executes the statement and returns all the data that is coincident.
   * 
   * @params model to be returned
   * @returns matching searches 
   */
  public function resultSet($model)
  {
    $this->execute();
    return $this->stmt->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $model);
  }
  
  /**
   * Executes the statement and returns a single coincidence.
   * 
   * @params model to be returned
   * @returns matching search
   */
  public function single($model)
  {
    $this->execute();
    $this->stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $model);
    return $this->stmt->fetch();
  }
  
  /**
   * Executes the statement and returns the number of coincidences or rows affected.
   * 
   * @returns rows affected
   */
  public function getRows()
  {
    $this->execute();
    return $this->stmt->rowCount();
  }
}
