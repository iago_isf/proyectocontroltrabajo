<?php
/**
 * Parent class for controllers which simplifies the model and view use.
 * 
 * @class Controller
 */
class Controller
{

  public function __construct()
  {

  }
  
  /**
   * This function searches a database model and if exists returns a new instance of that model.
   *
   * @params model name.
   * @returns model.
   */
  public function model ($modelo) 
  {
    if (file_exists('../app/models/' . $modelo . '.php')) { 
      require_once '../app/models/' . $modelo . '.php';
      return new $modelo();

    } else {
      die('The model was not located.');
    }
  }

  /**
   * This function searches a view and if exists requires it where was called.
   *
   * @params view name and an array of data to be displayed if needed.
   */
  public function view ($vista, $data=[]) 
  {
    if (file_exists('../app/views/' . $vista . '.php')) { 
      require_once '../app/views/' . $vista . '.php';

    } else {
      die('Unnable to locate the view.');
    }
  }
}
?>