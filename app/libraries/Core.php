<?php
/**
 * This class controls the url to redirect users wherever they are meant to be.
 * 
 * @class Core
 */
class Core
{
  protected $controller = "Paginas";
  protected $method = "index";
  protected $params = [];

  /**
   * When its instancied gets the url given and reads which controller, view and parameters the url has
   * to redirect the user to the proper view. 
   */
  public function __construct()
  {
    $url = $this->getUrl();

    // Checks the existence of the controller, if exists the controller is set.
    if (file_exists('../app/controllers/' . ucwords($url[0]) . '.php')) {
      $this->controller = ucwords($url[0]);
      unset($url[0]);
    }

    // Requires the controller to be used and instancies it.
    require_once '../app/controllers/' . $this->controller . '.php';
    $this->controller = new $this->controller;

    // Checks the method to be called from the controller.
    if (isset($url[1])) {
      if (method_exists($this->controller, $url[1])) {
        $this->method = $url[1];
        unset($url[1]);
      }
    }

    // The parameters left are parameters for the function and finally its all called together.
    $this->params = $url ? array_values($url) : [];
    call_user_func_array([$this->controller, $this->method], $this->params);
  }

  /**
   * This function gets the url and sanitices it to be easy to work with.
   *
   * @returns url sanitized array. 
   */
  public function getUrl()
  {
    if (isset($_GET['url'])) {
      $url = rtrim($_GET['url'], '/');
      $url = filter_var($url, FILTER_SANITIZE_URL);
      $url = explode('/', $url);

    } else {
      $url[0] = "Paginas";
    }

    return $url;
  }
}
?>