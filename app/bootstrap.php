<?php
if( !session_id() ) @session_start();
require_once 'vendor/autoload.php'; // Starts composer dependencies

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__); // Starts composer dependencie dotenv
$dotenv->safeLoad();

// Requires all the necesary code
require_once 'config/config.php';
require_once 'exceptions/FileException.php';
require_once 'helpers/utils.php';

spl_autoload_register(function ($className) {
  require_once 'libraries/'.$className.'.php';
});
?>