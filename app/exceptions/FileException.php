<?php
/**
 * This class allows create exceptions with custom messages.
 * 
 * @class Paginas
 * @extends exception
 */
class FileException extends Exception
{
  public function __construct(string $message)
  {
    parent::__construct($message);
  }
}
?>