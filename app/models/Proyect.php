<?php
/**
 * This class contains all the necesary queries to manipulate the data from the database related to the proyects.
 * 
 * @class Proyect
 */
class Proyect
{
  private $db;

  /**
   * The constructor instanced a new database for this model.
   */
  public function __construct()
  {
   $this->db = new Database(); 
  }

  /**
   * This function returns all the proyects depending on if the user wants to see the created ones by him or all of them.
   * 
   * @params user id and boolean for all proyects or created ones
   * @returns coincident results
   */
  public function getProyects($id, $created = false)
  {
    if($created){
      $this->db->query("SELECT 
      proyect.idproyect as proyect_id,
      proyect.name as proyect_name,
      proyect.description as proyect_description,
      user.name as user_name,
      user.surnames as user_surnames
      FROM proyect 
      INNER JOIN user on proyect.iduser = user.iduser
      WHERE proyect.iduser = :id");

      $this->db->bind(':id', $id);

    } else {
      $this->db->query("SELECT 
      proyect.idproyect as proyect_id,
      proyect.name as proyect_name,
      proyect.description as proyect_description,
      user.name as user_name,
      user.surnames as user_surnames
      FROM proyect 
      INNER JOIN user on proyect.iduser = user.iduser");
    }

    return $this->db->resultSet('Proyect');
  }

  /**
   * This function returns true or false depending on wether the proyect has tasks or not.
   * 
   * @params proyect id
   * @returns boolean
   */
  public function checkWorks($id){
    $this->db->query("SELECT *
      FROM proyect 
      INNER JOIN work on proyect.idproyect = work.idproyect
      WHERE proyect.idproyect = :id");
    $this->db->bind(':id', $id);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }

  /**
   * This function returns the information from a unic proyect but choosing between having all of its information, including
   * the tasks related to it or just the general information.
   * 
   * @params proyect id and boolean for all proyect info or simplified info
   * @returns coincident results
   */
  public function getProyectById($id, $all = false)
  {
    if($all){
      $this->db->query("SELECT
      proyect.idproyect as proyect_id,
      proyect.name as proyect_name,
      proyect.description as proyect_description,
      proyect.image as proyect_image,
      work.idwork as work_id,
      work.name as work_name,
      work.description as work_description,
      work.done as work_done,
      creator.email as creator_email,
      worker.email as worker_email
      FROM proyect 
      INNER JOIN work on proyect.idproyect = work.idproyect
      INNER JOIN user as creator on proyect.iduser = creator.iduser
      INNER JOIN user as worker on work.iduser = worker.iduser
      WHERE proyect.idproyect = :id");
      $this->db->bind(':id', $id);

    } else {
      $this->db->query("SELECT 
      proyect.idproyect as proyect_id,
      proyect.iduser as creator_id,
      proyect.name as proyect_name,
      proyect.description as proyect_description,
      proyect.image as proyect_image,
      creator.email as creator_email
      FROM proyect
      INNER JOIN user as creator on proyect.iduser = creator.iduser
      WHERE proyect.idproyect = :id");
      $this->db->bind(':id', $id);
    }
    
    return $this->db->resultSet('Proyect');
  }

  /**
   * This function creates a new proyect into the database.
   * 
   * @params array with the data
   * @returns true if the creation was succesful, false in other case
   */
  public function add($data)
  {
    $this->db->query("INSERT INTO proyect (name, description, iduser, image) VALUES (:name, :description, :iduser, :image)");
    $this->db->bind(':name', $data['name']);
    $this->db->bind(':description', $data['description']);
    $this->db->bind(':image', $data['image']);
    $this->db->bind(':iduser', $data['id_user']);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }

  /**
   * This function updates a proyect from the database.
   * 
   * @params array with the data
   * @returns true if creation was succesful, false in other case
   */
  public function update($data)
  {
    $this->db->query("UPDATE proyect SET name = :name, description = :description WHERE idproyect = :idproyect");
    $this->db->bind(':name', $data['name']);
    $this->db->bind(':description', $data['description']);
    $this->db->bind(':idproyect', $data['id_proyect']);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }

  /**
   * This function removes a proyect from the database
   * 
   * @params proyect id
   * @returns true if creation was succesful, false in other case
   */
  public function delete($id)
  {
    $this->db->query('DELETE FROM proyect WHERE idproyect = :id');
    $this->db->bind(':id', $id);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }
}
?>