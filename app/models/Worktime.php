<?php
/**
 * This class contains all the necesary queries to manipulate the data from the database related to the worktimes.
 * 
 * @class Worktime
 */
class Worktime
{
  private $db;

  /**
   * The constructor instanced a new database for this model.
   */
  public function __construct()
  {
   $this->db = new Database(); 
  }

  /**
   * This function returns all the worktimes related to a user
   * 
   * @params user id
   * @returns coincident results
   */
  public function getWorktimesByUser($id)
  {
    $this->db->query("SELECT * FROM worktime 
    INNER JOIN work on worktime.idwork = work.idwork
    WHERE work.iduser = :id");
    $this->db->bind(':id', $id);

    return $this->db->resultSet('Worktime');
  }

  /**
   * This function returns all the information from a task
   * 
   * @params work id
   * @returns coincident results
   */
  public function getUserIdFromWork($id)
  {
    $this->db->query("SELECT * FROM work WHERE idwork = :id");
    $this->db->bind(':id', $id);

    return $this->db->resultSet('Worktime');
  }

  /**
   * This function returns all the worktimes related to a task
   * 
   * @params work id
   * @returns coincident results
   */
  public function getWorktimes($id)
  {
    $this->db->query("SELECT * FROM worktime WHERE idwork = :id");
    $this->db->bind(':id', $id);

    return $this->db->resultSet('Worktime');
  }

  /**
   * This function updates a worktime setting its finnish time to the current time.
   * 
   * @params worktime id
   * @returns true if creation was succesful, false in other case
   */
  public function update($id)
  {
    $this->db->query("UPDATE worktime SET end = :endtime WHERE idworktime = :id");
    $this->db->bind(':id', $id);
    $this->db->bind(':endtime', actualDatetime());

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }

  /**
   * This function creates a new worktime with its start date as the current time and its end time as 0.
   * 
   * @params work id
   * @returns true if creation was succesful, false in other case
   */
  public function add($id)
  {
    $this->db->query("INSERT INTO worktime (start, end, idwork) VALUES (:start, :end, :idwork)");
    $this->db->bind(':idwork', $id);
    $this->db->bind(':start', actualDatetime());
    $this->db->bind(':end', '1000-01-01 00:00:00');

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }
}
?>