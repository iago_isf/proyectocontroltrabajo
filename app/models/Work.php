<?php
/**
 * This class contains all the necesary queries to manipulate the data from the database related to the tasks.
 * 
 * @class Work
 */
class Work
{
  private $db;

  /**
   * The constructor instanced a new database for this model.
   */
  public function __construct()
  {
   $this->db = new Database(); 
  }

  /**
   * This function returns all the tasks related to a user depending on:
   * 0 or default - all user tasks,
   * 1 - user created tasks,
   * 2 - work in progres user tasks
   * 
   * @params user id and/or int for find mode
   * @returns coincident results
   */
  public function getWorks($id, $created = 0)
  {
    if($created == 1){
      $this->db->query("SELECT
      work.idwork as work_id,
      work.name as work_name,
      work.description as work_description,
      proyect.name as proyect_name
      FROM work 
      INNER JOIN proyect on work.idproyect = proyect.idproyect
      WHERE proyect.iduser = :id");

    } else if($created == 2){
      $this->db->query("SELECT
      work.idwork as work_id,
      work.name as work_name,
      work.description as work_description,
      proyect.name as proyect_name
      FROM work 
      INNER JOIN proyect on work.idproyect = proyect.idproyect
      WHERE work.iduser = :id and work.done = 0");

    } else {
      $this->db->query("SELECT
      work.idwork as work_id,
      work.name as work_name,
      work.description as work_description,
      proyect.name as proyect_name
      FROM work 
      INNER JOIN proyect on work.idproyect = proyect.idproyect
      WHERE work.iduser = :id");
    }

    $this->db->bind(':id', $id);

    return $this->db->resultSet('Work');
  }

  /**
   * This function returns all the information from a task.
   * 
   * @params work id
   * @returns coincident results
   */
  public function getWorkById($id)
  {
    $this->db->query("SELECT
    work.idwork as work_id,
    work.iduser as user_id,
    work.name as work_name,
    work.description as work_description,
    work.done as work_done,
    proyect.name as proyect_name
    FROM work 
    INNER JOIN proyect on work.idproyect = proyect.idproyect 
    WHERE work.idwork = :id");

    $this->db->bind(':id', $id);

    return $this->db->resultSet('Work');
  }

  /**
   * This function returns all the information from all the worktimes related to a work.
   * 
   * @params work id
   * @returns coincident results
   */
  public function getWorkWorktimes($id)
  {
    $this->db->query("SELECT
    work.idwork as work_id,
    work.iduser as user_id,
    worktime.idworktime as worktime_id,
    worktime.start as worktime_start,
    worktime.end as worktime_end
    FROM work
    INNER JOIN worktime on work.idwork = worktime.idwork
    WHERE worktime.idwork = :id");

    $this->db->bind(':id', $id);

    return $this->db->resultSet('Work');
  }

  /**
   * This function returns all the proyects related to a user.
   * 
   * @params user id
   * @returns coincident results
   */
  public function getUserProyects($user_id)
  {
    $this->db->query("SELECT
    proyect.idproyect as proyect_id,
    proyect.name as proyect_name
    FROM proyect WHERE proyect.iduser = :id");
    $this->db->bind(':id', $user_id);

    return $this->db->resultSet('Work');
  }

  /**
   * This function returns all the users from the database.
   * 
   * @returns coincident results
   */
  public function getUsers()
  {
    $this->db->query("SELECT
    user.iduser as user_id,
    user.name as user_name,
    user.surnames as user_surnames
    FROM user");

    return $this->db->resultSet('Work');
  }

  /**
   * This function creates a new task into the database.
   * 
   * @params array with the data
   * @returns true if the creation was succesful, false in other case
   */
  public function add($data)
  {
    $this->db->query("INSERT INTO work (name, description, iduser, idproyect) VALUES (:name, :description, :iduser, :idproyect)");
    $this->db->bind(':name', $data['name']);
    $this->db->bind(':description', $data['description']);
    $this->db->bind(':iduser', $data['user']);
    $this->db->bind(':idproyect', $data['proyect']);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }

  /**
   * This function updates a task from the database.
   * 
   * @params array with the data
   * @returns true if creation was succesful, false in other case
   */
  public function update($data)
  {
    $this->db->query("UPDATE work SET name = :name, description = :description WHERE idwork = :idwork");
    $this->db->bind(':name', $data['name']);
    $this->db->bind(':description', $data['description']);
    $this->db->bind(':idwork', $data['id_work']);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }

  /**
   * This function updates a proyect to mark it as done
   * 
   * @params work id
   * @returns true if creation was succesful, false in other case
   */
  public function done($id)
  {
    $this->db->query("UPDATE work SET done = 1 WHERE idwork = :id");
    $this->db->bind(':id', $id);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }
}
?>