<?php
/**
 * This class contains all the necesary queries to manipulate the data from the database related to the users.
 * 
 * @class User
 */
class User
{
  private $db;

  /**
   * The constrctor instanced a new database for this model.
   */
  public function __construct()
  {
   $this->db = new Database(); 
  }

  /**
   * This function returns all the users from the database.
   * 
   * @returns all the data from the users
   */
  public function getUsers()
  {
    $this->db->query("SELECT * FROM user");
    
    return $this->db->resultSet('user');
  }

  /**
   * This function checks if there is any user with the same email in the database, if so returns true else returns false.
   * 
   * @params user email
   * @returns boolean
   */
  public function findUserByEmail($email)
  {
    $this->db->query('SELECT * FROM user WHERE email = :email');
    $this->db->bind(':email', $email);

    $coincidences = $this->db->getRows();

    if($coincidences >= 1){
      return true;
    } else {
      return false;
    }
  }

  /**
   * This function creates a new user into the database.
   * 
   * @params array with the data
   * @returns true if the creation was succesful, false in other case
   */
  public function register($data)
  {
    $this->db->query('INSERT INTO user (name, surnames, phone, email, password) VALUES (:name, :surnames, :phone, :email, :password)');
    $this->db->bind(':name', $data['name']);
    $this->db->bind(':surnames', $data['surnames']);
    $this->db->bind(':phone', $data['phone']);
    $this->db->bind(':email', $data['email']);
    $this->db->bind(':password', $data['password']);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }

  /**
   * This function checks if there is any user with the same email in the database and validates the password, if all its 
   * correct returns the user information otherwise returns false.
   * 
   * @params array with the data
   * @returns user information or false
   */
  public function login($data)
  {
    $this->db->query('SELECT * FROM user WHERE email=:email');
    $this->db->bind(':email', $data['email']);
    $logUser = $this->db->single('User');
    
    if(password_verify($data['password'], $logUser->password)){
      return $logUser;
    } else {
      return false;
    }
  }

  /**
   * This function updates an user to become a supervisor.
   * 
   * @params user id
   * @returns true if the update was succesful, false in other case
   */
  public function update($id)
  {
    $this->db->query('UPDATE user SET supervisor = 1 WHERE iduser = :id');
    $this->db->bind(':id', $id);

    if($this->db->getRows() >= 1){
      return true;
    } else {
      return false;
    }
  }
}
?>