<?php
/**
 * Proyects specific view with all its data.
 *
 * @params proyect id. 
 * @returns selected proyect information and its view. 
 */
function dump_die($variable)
{
  var_dump($variable);
  die();
}

/**
 * Redirects the user to a especified page.
 */
function urlHelper($url){
  header('Location:'. URLROOT . '/'. $url );
  return;
}

/**
 * Checks if the user is logged by checking the existence of the user cookies.
 *
 * @returns true if the cookie exists or false if not. 
 */
function isLoggedIn(){
  if(isset($_SESSION['user_id'])){
    return true;
  } else {
    return false;
  }
}

/**
 * Checks the url to do changes over the client view.
 *
 * @params page name.
 * @returns css class names for changes on the view. 
 */
function isActive($option){
  switch ($option){
    case 'home':
      if (strpos($_SERVER['REQUEST_URI'], $option)){
        return 'border-bottom border-dark border-2';
      }
      break;
    case 'proyects':
      if (strpos($_SERVER['REQUEST_URI'], $option)){
        return 'border-bottom border-dark border-2';
      }
      break;
    case 'works':
      if (strpos($_SERVER['REQUEST_URI'], $option)){
        return 'border-bottom border-dark border-2';
      }
      break;
    default:
      return false;
      break;
  }
}

/**
 * Formats the php date into the sql format
 */
function actualDatetime()
{
  $date = date('c');
  $date = explode('T', $date);
  $date[1] = explode('+', $date[1]);
  $date = $date[0] . ' ' . $date[1][0];
  return $date;
}

/**
 * Checks if the user has supervisor privileges.
 *
 * @returns true or false depending on the privileges cookie value. 
 */
function supervisor()
{
  switch($_SESSION['user_supervisor']){
    case 0:
      return false;
      break;
    case 1:
      return true;
      break;
  }
}
?>