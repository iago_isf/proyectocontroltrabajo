/**
 * This event sets the footer as fixed bottom whenever the page has not enough content to 
 * set it at the total bottom of the page.
 */
window.addEventListener('load', ()=>{
  if(window.innerHeight >= document.getElementsByTagName('body')[0].offsetHeight){
    document.getElementsByTagName('footer')[0].style.position = 'fixed';
    document.getElementsByTagName('footer')[0].style.bottom = 0;
    document.getElementsByTagName('footer')[0].style.width = '100%';
  }
});
